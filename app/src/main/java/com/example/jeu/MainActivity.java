package com.example.jeu;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button boutonPlay;
    private int max_score;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // bouton pour commencer le jeu
        boutonPlay = findViewById(R.id.MainAct_Btn_Play);
        boutonPlay.setOnClickListener(this);

        //resetScores();
        initializeScores(); //lit le score maximum actuel

        TextView maxScoreDisplay = findViewById(R.id.maxScore);
        String msg = getString(R.string.max_score, max_score);
        maxScoreDisplay.setText(msg);
    }

    @Override
    public void onClick(View v) {
        Intent i = new Intent();
        if (v == boutonPlay) { //passe a l'activite de jeu (PlayActivity)
            i.setClass(getApplicationContext(), PlayActivity.class);
            startActivity(i);
            finish();
        }
    }

    private void initializeScores() { //lit ou initialise le score maximum (SharedPreferences)
        SharedPreferences sharedPreferences = this.getSharedPreferences("scores", Context.MODE_PRIVATE);
        if (sharedPreferences != null) {
            max_score = sharedPreferences.getInt("maxScore", 0);
        }
    }

    private void resetScores() { //reinitialisation de SharedPreference
        SharedPreferences sharedPreferences = this.getSharedPreferences("scores", Context.MODE_PRIVATE);
        sharedPreferences.edit().clear().apply();
    }
}