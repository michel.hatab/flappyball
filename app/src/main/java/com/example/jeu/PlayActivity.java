package com.example.jeu;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

public class PlayActivity extends AppCompatActivity {

    Boule boule;
    Rectangles rectangles;
    static Timer timer;

    private int cur_score = 0; //score actuel
    private int max_score; //score maximum

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_play);
        ConstraintLayout layout = findViewById(R.id.layoutPlayActivity);

        //initialisation de la boule
        boule = new Boule(getApplicationContext());
        layout.addView(boule);

        //initialisation des rectangles
        rectangles = new Rectangles(getApplicationContext());
        layout.addView(rectangles);

        //affichage du score actuel
        TextView scoreDisplay = findViewById(R.id.scoreDisplay);
        String msg_score = getString(R.string.score_play, cur_score);
        scoreDisplay.setText(msg_score);

        //lit score maximum (SharedPreferences)
        SharedPreferences sharedPreferences = this.getSharedPreferences("scores", Context.MODE_PRIVATE);
        max_score = sharedPreferences.getInt("maxScore", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        //controle des collisions
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            Boolean alreadyScore = false; //verifie si un point a deja ete comptabilise

            @Override
            public void run() {

                Boolean right_x = boule.posX - boule.rayon < rectangles.posRight;
                Boolean left_x = boule.posX + boule.rayon > rectangles.posLeft;
                Boolean upper_y = boule.posY - boule.rayon < rectangles.ouverture;
                Boolean lower_y = boule.posY + boule.rayon > rectangles.ouverture + rectangles.vide;

                if (right_x && left_x) { //boule dans la meme position X des rectangles

                    if (upper_y || lower_y) { //la collision a eu lieu

                        //arrete les timers
                        Boule.timer.cancel();
                        Rectangles.timer.cancel();
                        timer.cancel();

                        //verifie si c'est le score maximum
                        if (cur_score > max_score) {
                            editor.putInt("maxScore", cur_score);
                        }
                        editor.apply();

                        //activite suivante (ScoreActivity)
                        Intent i = new Intent(PlayActivity.this, ScoreActivity.class);
                        i.putExtra("score", cur_score); //transmet le score actuel a l'activite suivante
                        startActivity(i);
                        finish();

                    } else {

                        if (!alreadyScore) { //le point est compte

                            alreadyScore = true;
                            cur_score++;
                            String msg = getString(R.string.score_play, cur_score);
                            scoreDisplay.setText(msg);

                        }
                    }
                } else {
                    alreadyScore = false;
                }
            }
        }, 500, 10);

    }

    @Override
    public void onBackPressed() { //le bouton back est appuye
        super.onBackPressed();

        //arrete les timers
        Boule.timer.cancel();
        Rectangles.timer.cancel();
        PlayActivity.timer.cancel();

        //activite suivante
        Intent i = new Intent();
        i.setClass(getApplicationContext(), MainActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();

        //arrete les timers
        Boule.timer.cancel();
        Rectangles.timer.cancel();
        PlayActivity.timer.cancel();
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        //activite suivante (MainActivity)
        Intent i = new Intent();
        i.setClass(getApplicationContext(), MainActivity.class);
        startActivity(i);
        finish();
    }
}
