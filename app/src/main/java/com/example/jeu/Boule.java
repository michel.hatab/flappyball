package com.example.jeu;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.view.MotionEvent;
import android.view.View;

import java.util.Timer;
import java.util.TimerTask;

public class Boule extends View{
    Paint pinceau;
    int rayon = 50; //rayon de la boule
    int posX, posY; //coordonnees de la boule
    int vY = 10; //vitesse initiale
    boolean posYinit = true; //initialisation de la position Y
    double gravity_factor = 0.05; //facteur de gravité ou acceleration
    int no_touch; //compteur de trames consecutives dans lesquelles il n'y avait pas de contact sur l'ecran
    static Timer timer;
    MediaPlayer lecteurMedia; //on cree Mediaplayer pour ajouter de la musique

    public Boule (Context context){
        super(context);

        lecteurMedia = MediaPlayer.create(context, R.raw.sound_touch);
        pinceau = new Paint();
        pinceau.setColor(Color.RED); //boule rouge

        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                int deltaY = (int) (vY + no_touch*gravity_factor*vY); //deplacement de la boule
                if ((posY+deltaY > 0) && (posY+deltaY < getHeight())){ //si la balle ne quitte pas l'ecran
                    posY += deltaY;
                    no_touch++;
                }
                postInvalidate(); //appelle invalidate sur the thread de l'app
            }
        }, 0, 25);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        posX=getWidth()/3; //position X constante
        if (posYinit){ //initialisation de la position Y
            posY=getHeight()/2;
        }
        posYinit=false;
        canvas.drawCircle(posX,posY,rayon,pinceau);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        lecteurMedia.start(); //joue son
        no_touch = 0; //met no_touch a 0
        int deltaYb = -150; //rebondissement de la boule apres avoir tape sur l'ecran
        if (posY+deltaYb < 0) {
            posY=0;
        }else{
            posY += deltaYb;
        }
        invalidate();
        requestLayout();
        return super.onTouchEvent(event);
    }
}
