package com.example.jeu;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ScoreActivity extends AppCompatActivity implements View.OnClickListener {

    private Button boutonRestart; //bouton pour redemarrer le jeu

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        boutonRestart = findViewById(R.id.buttonRestart);
        boutonRestart.setOnClickListener(this);

        //lit le score actuel de l'activite precedente
        Intent appelant = getIntent();
        int cur_score = appelant.getIntExtra("score",0);

        //affiche le score actuel
        TextView affichageScore = findViewById(R.id.finalScore);
        String msg_score = getString(R.string.your_score_is, cur_score);
        affichageScore.setText(msg_score);

        //ecrit et lit dans scores si cur_score < max_score
        //enregistrement sous forme de "cle/valeur" dans un fichier "scores.xml" et on met private pour qu'il reste pour notre application
        SharedPreferences sp = getSharedPreferences("scores", Context.MODE_PRIVATE);
        TextView affichageHighScore = findViewById(R.id.highestScore);
        int max_score = sp.getInt("maxScore", 0);

        //affiche le score maximum
        String msg_max;
        if (cur_score >= max_score) {
            msg_max = getString(R.string.its_your_highest_score);
        } else {
            msg_max = getString(R.string.max_score, max_score);
        }
        affichageHighScore.setText(msg_max);
    }

    @Override
    public void onClick(View v) {
        Intent i = new Intent();
        if (v == boutonRestart) {

            //arrete les timers
            Boule.timer.cancel();
            Rectangles.timer.cancel();
            PlayActivity.timer.cancel();

            //activite suivante (PlayActivity)
            i.setClass(getApplicationContext(), PlayActivity.class);
            startActivity(i);
            finish();
        }
    }

    @Override
    public void onBackPressed() { //le bouton back est appuye
        super.onBackPressed();

        //arrete les timers
        Boule.timer.cancel();
        Rectangles.timer.cancel();
        PlayActivity.timer.cancel();

        //activite suivante (Main Activity)
        Intent i = new Intent();
        i.setClass(getApplicationContext(), MainActivity.class);
        startActivity(i);
        finish();
    }
}