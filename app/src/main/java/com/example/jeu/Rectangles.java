package com.example.jeu;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import java.sql.Time;
import java.util.Timer;
import java.util.TimerTask;

public class Rectangles extends View {

    Paint pinceau;
    int posLeft, posRight; //coordonnees des rectangles
    boolean rectInit = true; //initialisation des rectangles

    int vide = 600; //longuer de l'ouverture
    int rectWidth = 150; //largeur des rectangles
    int ouverture; //coordonee superieure de l'ouverture

    static Timer timer;
    int deltaX = -10; //vitesse des rectangles

    public Rectangles(Context context) {
        super(context);
        pinceau = new Paint();
        pinceau.setColor(Color.BLUE); //rectangles bleus

        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                //deplacement des rectangles
                posLeft += deltaX;
                posRight += deltaX;
                postInvalidate(); // appelle invalidate sur the thread de l'app
            }
        }, 0, 25);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (rectInit) { //initialisation des rectangles
            ouverture = (int)(Math.random()*(getHeight()-vide));
            posLeft = getWidth();
            posRight = getWidth()+rectWidth;
        }
        canvas.drawRect(posLeft, 0, posRight, ouverture, pinceau);
        canvas.drawRect(posLeft, ouverture+vide, posRight, getHeight(), pinceau);

        if (posRight < 0) { //moment ou l'on dessine de nouveaux rectangles
            rectInit = true;
        } else {
            rectInit = false;
        }
    }
}

